<?php
//check if the database connection is done properly
$mycon = databaseConnect();


//decare object for database queries
$dataOP = New dataOperation();

//begin declare the class shoppingcart
$shoppingcart = New ShoppingCart();

$couponshoppingcart = New CouponShoppingCart();

//delivery fee per each cart
$deliveryfee = 50;

//gross total for all the items in the cart
$grosstotal = 0;

//get the details of the cart
$cart_details = $dataOP->cart_getbyid($mycon);

//perform the add item to cart function
if (isset($_GET['action']) && $_GET['action'] == 'addItem') $shoppingcart->addItem($_GET['name'], $_GET['quantity'], $_GET['price']);
elseif (isset($_GET['action']) && $_GET['action'] == 'checkout') $shoppingcart->checkout($_GET['paid_amount'], $_GET['original_amount']);
elseif (isset($_GET['delete']) && $_GET['delete'] != '') $shoppingcart->removeItem($_GET['delete']);
elseif (isset($_GET['coupon']) && $_GET['coupon'] == 'SUPERMART_DEV') $couponshoppingcart->addCoupon($_GET['coupon']);


//function to connect to the database - mysql
function databaseConnect()
{
	$MYSQL_Server = 'localhost';
	$MYSQL_Database = 'carts';
	$MYSQL_Username = 'root';
	$MYSQL_Password = '';

	$mycon = new PDO("mysql:host=$MYSQL_Server;dbname=$MYSQL_Database;charset=utf8", "$MYSQL_Username", "$MYSQL_Password");	
	$mycon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$mycon->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);	
	return $mycon;
}

//Displays a javascript alert
function showAlert($message)
{
?>
	<script language="javascript">
    	alert("<?php echo str_replace("**","\\n",$message) ?>");
    </script>
<?php
}

//Opens the requested page
function openPage($page)
{
?>
	<script language="javascript">
    	window.parent.window.parent.window.parent.document.location.href="<?php echo $page ?>";
    </script>
<?php
}



//class for shoppingcart
class ShoppingCart
{


	//add to cart function
	function addItem($name, $quantity, $price)
	{
	 
	  $mycon = databaseConnect();
	  $dataOP = New dataOperation();
	  
	  //check if the cart in the product still exists
	  $cart_exists = $dataOP->cart_getbyname($mycon, $name);
	  if ($cart_exists)
	  {
	    //$price = $price + $cart_exists['price'];
	    $quantity = $quantity + $cart_exists['quantity'];
	    $cart_update = $dataOP->cart_update($mycon, $name, $price, $quantity, $cart_exists['cart_id']);
	    if (!$cart_update)
	  {
	    showAlert('Unable to process your cart. Please try again.');
	    return;
	  }
	}
	  else {
	     $cart_add = $dataOP->cart_add($mycon, $name, $price, $quantity);
	  if (!$cart_add)
	  {
	    showAlert('Unable to process your cart. Please try again.');
	    return;
	  }
	  }
	 

	 //if all is successful
	 showAlert('Item has been added to cart.');
	 openPage('/');
	}

	function checkout($amount_paid, $original_amount)
	{
		$mycon = databaseConnect();
	  	$dataOP = New dataOperation();

	  	//delete all the cart items
	  	$cart_items_delete = $dataOP->carts_delete_all($mycon);
	  	if (!$cart_items_delete)
		  {
		    showAlert('Unable to clear the carts. Please try again later.');
		    return;
		    
		}
		//return the balance
		$balance = $amount_paid - $original_amount;
		  
		  showAlert('Check out successful. You have a balance of '.$balance.' naira.');
		  openPage('/');

	}

	function removeItem($cart_id)
	{
		  $mycon = databaseConnect();
	  	  $dataOP = New dataOperation();
		  
		  //first check if the cart id still exists
		  $cart_exists = $dataOP->carts_getbyid($mycon, $cart_id);
		  if (!$cart_exists)
		  {
		    showAlert('This item do not exists anymore. Please try again. Refresh your page.');
		    openPage('/');
		    
		  }
		  
		  //if it exists
		  $cart_delete = $dataOP->carts_delete($mycon, $cart_id);
		  if (!$cart_delete)
		  {
		    showAlert('Unable to delete this item. Please try again later.');
		    return;
		    
		  }
		  
		  showAlert('This item was deleted successfully.');
		  openPage('/');
	}

}

//coupon shopping cart
class CouponShoppingCart extends shoppingcart
{

	function addCoupon($coupon_code)
	{
	  $mycon = databaseConnect();
	  $dataOP = New dataOperation();
	  
	  //check if the cart in the product still exists
	  $coupon_exists = $dataOP->coupon_getbyname($mycon, $coupon_code);
	  if (!$coupon_exists)
	  {
	     $coupon_add = $dataOP->coupon_add($mycon, $coupon_code);
		  if (!$coupon_add)
		  {
		    showAlert('Unable to add coupon. Please try again.');
		    return;
		  }
	  }
	 

	 //if all is successful
	 showAlert('Coupon Added. Items gross total has been reduced by 1,000 naira.');
	 openPage('/');
	}
}


//database query class called data
class dataOperation
{

	function cart_getbyname($mycon, $name)
    {
      $sql = "SELECT * FROM `carts` WHERE `name` = :name LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":name", $name);
      $myrec->execute();
        
      if ($myrec->rowCount() < 1) return false;
        
      return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function cart_update($mycon, $name, $amount, $quantity, $cart_id)
	{
	  $thedate = date("Y-m-d H:i:s");
	  $sql = "UPDATE `carts` SET
	          `name` = :name
	          ,`price` = :price
	          ,`quantity` = :quantity
	          ,`updatedon` = :updatedon WHERE `cart_id` = :cart_id LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":price", $amount, PDO::PARAM_STR);
      $myrec->bindValue(":name", $name,PDO::PARAM_STR);
      $myrec->bindValue(":quantity", $quantity, PDO::PARAM_STR);
      $myrec->bindValue(":cart_id", $cart_id,PDO::PARAM_STR);
      $myrec->bindValue(":updatedon", $thedate,PDO::PARAM_STR);
      
      
      if (!$myrec->execute()) return false;
      
	      return true;
	 }

	 function cart_add($mycon, $name, $price, $quantity)
	  {
	    $thedate = date("Y-m-d H:i:s");
	      $sql = "INSERT INTO `carts` SET
	          `name` = :name
	          ,`price` = :price
	          ,`quantity` = :quantity
	          ,`createdon` = :createdon
	          ,`updatedon` = :updatedon";
	      $myrec = $mycon->prepare($sql);
	      $myrec->bindValue(":price", $price, PDO::PARAM_STR);
	      $myrec->bindValue(":name", $name,PDO::PARAM_STR);
	      $myrec->bindValue(":quantity", $quantity, PDO::PARAM_STR);
	      $myrec->bindValue(":createdon", $thedate,PDO::PARAM_STR);
	      $myrec->bindValue(":updatedon", $thedate,PDO::PARAM_STR);
	      $myrec->execute();
	      
	      if ($myrec->rowCount() < 1) return false;
	      
	      return $mycon->lastInsertId();
	  }


	 function cart_getbyid($mycon)
	  {
	    $sql = "SELECT * FROM `carts`";
	    $myrec = $mycon->prepare($sql);
	    $myrec->execute();
	        
	        
	    return $myrec->fetchAll(PDO::FETCH_ASSOC);
	  }


	function carts_delete($mycon,$cart_id)
    {
      $sql = "DELETE FROM `carts` WHERE `cart_id` = :cart_id LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":cart_id", $cart_id);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function carts_delete_all($mycon)
    {
      $sql = "DELETE  FROM `carts`";
      $myrec = $mycon->prepare($sql);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function coupon_delete_all($mycon)
    {
      $sql = "DELETE  FROM `coupons`";
      $myrec = $mycon->prepare($sql);
      $myrec->execute();

      if ($myrec->rowCount() < 1) return false;

      return true;
    }

    function carts_getbyid($mycon, $cart_id)
    {
      $sql = "SELECT * FROM `carts` WHERE `cart_id` = :cart_id LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":cart_id", $cart_id);
      $myrec->execute();
        
      if ($myrec->rowCount() < 1) return false;
        
      return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function coupon_getbyname($mycon, $coupon_code)
    {
      $sql = "SELECT * FROM `coupons` WHERE `code` = :code LIMIT 1";
      $myrec = $mycon->prepare($sql);
      $myrec->bindValue(":code", $coupon_code);
      $myrec->execute();
        
      if ($myrec->rowCount() < 1) return false;
        
      return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    

	 function coupon_add($mycon, $coupon_code)
	  {
	    $thedate = date("Y-m-d H:i:s");
	      $sql = "INSERT INTO `coupons` SET
	          `code` = :code
	          ,`createdon` = :createdon";
	      $myrec = $mycon->prepare($sql);
	      $myrec->bindValue(":code", $coupon_code, PDO::PARAM_STR);
	      $myrec->bindValue(":createdon", $thedate,PDO::PARAM_STR);
	      $myrec->execute();
	      
	      if ($myrec->rowCount() < 1) return false;
	      
	      return $mycon->lastInsertId();
	  }
}

//apply the coupon by reducing the gross total amount by 1,0000
$getCoupon = $dataOP->coupon_getbyname($mycon, 'SUPERMART_DEV');
if ($getCoupon)
{
	$grosstotal -= 1000;
}
//check if gross total is less than 0
if ($grosstotal < 0)
{
	$deleteCoupon = $dataOP->coupon_delete_all($mycon);
	openPage('/');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Ecommerce System</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap.css" rel="stylesheet">
    <style type="text/css">
    	.table 
    	{
    		border: 1px solid #444444;
    		padding: 20px;
    		margin: 20px;
    	}

    	.table tbody th {
    		border: 1px solid #444444;
    		margin:15%;
    	}

    	div .container .center h2 
    	{
    		margin: 10px;
    	}

    	.proceedtocheckout
    	{
    		margin: 10px;
    		font-weight: bold;
    	}
    </style>
</head>
<body>
    	<!-- Page Content -->
        <div class="container center">
        	<h2 class="text-center">SUPERMART ECOMMERCE NIGERIA <br /><br />
					Built with PHP </h2> <br /><br/>
        	<h3>Lists of all Products </h3>
        	<hr />
        	<div class="center">
	        	<table class="table">
	        		<thead>
	        			<th> S/N </th>
	        			<th>Product Name </th>
	        			<th>Quantity</th>
	        			<th>Price </th>
	        			<th>Total </th>
	        			<th>Action </th>
	        		</thead>
	        		<tbody>
	        			<tr>
	        				<td>1.</td>
	        				<td> Wrist Watch </td>
	        				<td><input class='form-control' type="number" value="1" id='quantity1' onkeyup="updateItem(this.value, '1')"> </td>
	        				<td>&#8358;<span id='price1'>2500</span></td>
	        				<td>&#8358;<span id='amount1'>2500 </span></td>
	        				<td><button type="button" class="btn btn-primary" onclick="addItem('Wrist Watch', '2500', '1')">Add to cart </button>
	        			</tr>
	        			<tr>
	        				<td>2.</td>
	        				<td> Shirts </td>
	        				<td><input class='form-control' type="number" value="1" id='quantity2' onkeyup="updateItem(this.value, '2')"> </td>
	        				<td>&#8358;<span id='price2'>2000</span></td>
	        				<td>&#8358;<span id='amount2'>2000 </span></td>
	        				<td><button type="button" class="btn btn-primary" onclick="addItem('Shirts', '2000', '2')"> Add to cart </button>
	        			</tr>
	        			<tr>
	        				<td>3.</td>
	        				<td> Trousers </td>
	        				<td><input class='form-control' type="number" value="1" id='quantity3' onkeyup="updateItem(this.value, '3')"> </td>
	        				<td>&#8358;<span id='price3'>4500</span></td>
	        				<td>&#8358;<span id='amount3'>4500 </span></td>
	        				<td><button type="button" class="btn btn-primary" onclick="addItem('Trousers', '4500', '3')">Add to cart </button>
	        			</tr>
	        			<tr>
	        				<td>4.</td>
	        				<td> Office Chairs </td>
	        				<td><input class='form-control' type="number" value="1" id='quantity4' onkeyup="updateItem(this.value, '4')"> </td>
	        				<td>&#8358;<span id='price4'>5750</span></td>
	        				<td>&#8358;<span id='amount4'>5750 </span></td>
	        				<td><button type="button" class="btn btn-primary" onclick="addItem('Office Chairs', '5750', '4')">Add to cart </button>
	        			</tr>
	        			
	        		</tbody>
	        		<tfoot>
	        			<th> S/N </th>
	        			<th>Product Name </th>
	        			<th>Quantity</th>
	        			<th>Price </th>
	        			<th>Total </th>
	        			<th>Action </th>
	        		</tfoot>
	        	</table>
	        </div>
	        <br /><br />
	        <h3>Items in Cart </h3>
        	<hr />
        	<div class="center">
	        	<table class="table">
	        		<thead>
	        			<th> S/N </th>
	        			<th>Product Name </th>
	        			<th>Price (Quantity)</th>
	        			<th>Delivery fee</th>
	        			<th>Net Total </th>
	        			<th>Action </th>
	        		</thead>
	        		<tbody>
	        			 <?php
                  
			                $count = 0;
			                $amount = 0;
			                if ($cart_details != null)
			                {
			                foreach($cart_details as $item => $key)
			                {
			                  $cart_id = $key['cart_id'];
			                 $netamount = $key['price']  * $key['quantity'] + $deliveryfee;
			                 $grosstotal += $netamount;
			             ?>
	        			<tr>
	        				<td><?php echo ++$count; ?></td>
	        				<td><?php echo $key['name']; ?></td>
	        				<td>&#8358;<span><?php echo $key['price']; ?></span> (<?php echo $key['quantity'] ?>)</td>
	        				<td>&#8358;<span><?php echo $deliveryfee; ?></span></td>
	        				<td>&#8358;<span><?php echo $netamount; ?> </span></td>
	        				<td><button type="button" class="btn btn-primary" onclick="if(confirm('Are you sure you want to remove this item?')) location.href='?delete=<?php echo $cart_id ?>'">Remove Item </button> </td>
	        			</tr>
	        			<?php
	        				}	
	        			}
	        				else {
	        				?>
	        			<tr>
	        				<td colspan="6" class="text-center" style="font-style: italic;">Nothing in cart</td>
	        			</tr>
	        			<?php
	        				}
	        				?>
	        		</tbody>
	        		<tfoot>
	        			<th colspan="4"> Gross Total </th>
	        			<th colspan="2">&#8358;<span class="grosstotal"><?php echo $grosstotal; ?></span></th>
	        		</tfoot>
	        	</table>
	        </div>
	        <br /><br />
	        <div class="text-center">
	        	<p>Apply Coupon: </p>
	        	<input type="text" id="applycoupon" class="form-control" value="<?php echo $getCoupon['code']; ?>" <?php if ($getCoupon['code'] != '') echo 'readonly'; ?>>
	        	<br/>
	        	 <button type="button" class="btn btn-primary" onclick="applyCoupon(<?php echo $grosstotal; ?>)">Apply </button>
	        </div>

	        <div class="text-center proceedtocheckout">
	        	
	        	 <button type="button" class="btn btn-lg btn-success" onclick="checkOut(<?php echo $grosstotal; ?>)">Check Out &#8358;<span class="grosstotal"><?php echo $grosstotal; ?></span></button>
	        </div>
            
            <!-- /.container-fluid -->
            <footer class="footer text-center"> <?php echo date("Y"); ?> &copy; Built By Shobowale Oluwadamilare (oluwadamilareshobowale@gmail.com)</footer>
        </div>
    

    <!-- jQuery -->
    <script src="jquery.js"></script>
     <script type='text/javascript'>
      function updateItem(value, row_id)
    {
      // var totalamount = $('.totalamount').html();
      if (value <= 0 || value == '')
      {
        value = 1;
        $(this).val(value);
        alert('Invalid quantity.');
        return;
      }
  		
  	//get the price of each item
  	price = $('#price'+row_id).html();
  	//calculate the corresponding total amount
  	totalamount = value * parseInt(price);
  	$('#amount'+row_id).html(totalamount);
       return; 
    }

    //add to cart
     function addItem(name, price, row_id)
	{
	  quantity = $('#quantity'+row_id).val();
	  location.href='?name='+name+'&price='+price+'&quantity='+quantity+'&action=addItem';
	}

	function applyCoupon(totalamount)
	{
		coupon = $('#applycoupon').val();
		if (coupon !== 'SUPERMART_DEV')
		{
			alert('Invalid Coupon Code. Try another coupon');
			$('#applycoupon').val('');
		}
		else if (totalamount <= 1000 && coupon === 'SUPERMART_DEV')
		{
			alert('Please purchase items that is above 10000.');
		}
		else
		{
			location.href='?coupon='+coupon;
		}

		return;
	}

		function checkOut(amount_paid)
		{
			amount_entered = prompt("You are paying: ", amount_paid);
			if (amount_entered < amount_paid)
			{
				alert('“Insufficient fund');
				return;
			}
			location.href='?paid_amount='+amount_entered+'&original_amount='+amount_paid+'&action=checkout';
		}
  </script>
</body>
</html>
