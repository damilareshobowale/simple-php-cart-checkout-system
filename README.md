**Simple PHP CaRT & Checkout System**

This is a simple php ecommerce system that involves cart, checkout and payment functionalities


---

## Files Inside

Here are the items inside

1. carts.sql - A SQL file for storage of information, import into phpmyadmin.
2. bootstrap.css - Bootstrap css file.
3. index.php - Run this file on the server.
4. Jquery.js - Jquery library
5. preview.png - a preview screenshot of the app

---

## How to Install

Follow this steps to install

1. Create a database name called carts
2. import carts.sql into the database name you created
3. configure your database parameters, you can edit it the databaseConnect() function in index.php
4. Run index.php on php server and then open your browser
5. You have successfully install the applications

If you have any enquiries or information, please reach me via email: oluwadamilareshobowale@gmail.com
